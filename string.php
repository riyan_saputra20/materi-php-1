<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> halaman string</title>
</head>
<body>
    <h1>Contoh string</h1>
<?php
    echo " <h3> Contoh String nomor 1 </h3> " ;

    $contohPertama = "Assalamualaikum perkenalkan nama saya riyan saputra" ;
    echo "<p> contoh kalimat 1 = " . $contohPertama . "</p>";
    echo "<p> Panjang string kalimat 1 = " . strlen($contohPertama) . "</p>";
    echo "<p> Jumlah kata kalimat 1 = " . str_word_count($contohPertama) . "</p>" ;

    echo "<h3> Contoh String nomor 2 </h3>" ;

    $contohKedua = "Hello World";
    echo "<p> Contoh kalimat  2 = " . $contohKedua . "</p>";
    echo "<p> kata pertama kalimat 2 = " . substr($contohKedua,0,5) . "</p>" ;
    echo "<p> kata kedua kalimat 2 = " . substr($contohKedua,6,5) . "</p>" ;

    echo "<h3> Contoh String nomor 3 </h3>" ;

    $contohKetiga = "Selamat Pagi" ;
    echo "Contoh Kalimat 3 = " . $contohKetiga . "</p>" ;
    echo "Ganti string kalimat 3 = " . substr_replace("Pagi", "Malam" , $contohKetiga) ;
?>
    
</body>
</html>