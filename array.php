<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Array</title>
</head>
<body>
    <h1> Contoh Soal Array </h1>
    <?php

    echo "<h3>Contoh 1 Array </h3>" ;

    $trainer = ["riyan", "saputra", "abunya", "khadijah", "qurrota" ];
    print_r($trainer);

    echo "<h3>Contoh 2 Array </h3>" ;

    echo "<h6>Total Trainer : " . count($trainer). "</h6>" ;
    echo "<ol>";
    echo "<li>" . $trainer[0] . "</li>" ;
    echo "<li>" . $trainer[1] . "</li>" ;
    echo "<li>" . $trainer[2] . "</li>" ;
    echo "<li>" . $trainer[3] . "</li>" ;
    echo "<li>" . $trainer[4] . "</li>" ;
    echo "</ol>";

    echo "<h3>Contoh 3 Array </h3>";

    $biodata = [
        ["nama"=>"riyan", "umur" => 10, "kota" => "sempor", "Status" => "Kontrak"],
        ["nama"=>"saputra","umur" => 20, "kota" =>"raya", "Status" => "Tetap"],
        ["nama"=>"abunya", "umur" =>30, "kota" =>"perumnas 2", "Status" => "Kontrak"],
        ["nama"=>"khadijah","umur" => 40,"kota" => "karawaci", "Status" => "Tetap"],
        ["nama"=>"qurrota","umur" => 50, "kota" =>"tangerang", "Status" => "Kontrak"],

    ];

    echo "<pre>";

    print_r($biodata);

    echo "</pre>";

    ?>

    
</body>
</html>